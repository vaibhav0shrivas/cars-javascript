// ==== Problem #2 ====
// The dealer needs the information on the last car in their inventory. Execute a function to find what the make and model of the last car in the inventory is?  Log the make and model into the console in the format of: 
"Last car is a *car make goes here* *car model goes here*"

const inventory = require('./cars.cjs');


function displayInformationLastCar(inventory){
    let carId = inventory.length;

    if(carId>50 || carId<1 || Number.isInteger(carId)==false){
        console.log("Car inventory is empty.");
        return;
    }
    console.log("Last car is a",inventory[carId-1].car_year,inventory[carId-1].car_make,inventory[carId-1].car_model);
    return;

}


module.exports = displayInformationLastCar;