// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.

const inventory = require('./cars.cjs');
const getAllYears  = require('./problem4.cjs');
  
  

function carsOlderThenYear(inventory,year=2000){
    
    if(Number.isInteger(year)==false || year< 1886 || (year > new Date().getFullYear())){
        console.log("Please Enter a valid year.",year,"is not a valid year.");
        return;
    }
    let yearsOfAllCars = getAllYears(inventory);
    if(inventory.length<1){
        console.log("Car inventory is empty, nothing to display.");
        return yearsOfAllCars;
    }
     
    let result = [];
    for( let i =0; i< yearsOfAllCars.length;i++){
        
        if(yearsOfAllCars[i]<year){
            result.push(yearsOfAllCars[i]);
        }
        
    }


    if(result.length < 1){
        console.log("There are no cars older than the year",year,".");
        return result;
    }
    else if(result.length == 1){
        console.log("There is 1 car older than the year",year,".");
    }
    else{
        console.log("There are",result.length,"cars older than the year",year,".");
    }     
    
    console.log(result);
    return result;

}


module.exports = carsOlderThenYear;