// ==== Problem #1 ====
// The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. Then log the car's year, make, and model in the console log in the format of: 
// "Car 33 is a *car year goes here* *car make goes here* *car model goes here*"




function displayInformationOfId(inventory = null,carId = null){
    let car =[]; 
    if (Array.isArray(inventory) == false || carId == null || typeof inventory != 'object' || inventory == null || inventory.length == 0 ){
        return car;
    }
    
    
    for(let index = 0; index < inventory.length; index++){

        if(inventory[index].id == carId){
        
        car.push(inventory[index]);
        console.log("Car",carId,"is a",inventory[index].car_year,inventory[index].car_make,inventory[index].car_model);
        return car;
        
        }
        
    }
    // console.log("Car with id",carId," does not exist in the inventory.");
    return car;

    

}


module.exports = displayInformationOfId;