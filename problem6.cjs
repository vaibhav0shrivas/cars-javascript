// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.


const inventory = require('./cars.cjs');
  
  

function getBMWAndAudi(inventory){
    
    let result = [];
    if(inventory.length<1){
        console.log("Car inventory is empty, nothing to display.");
        return result;
    }
     
   
    for( let i =0; i< inventory.length;i++){
        
        if(inventory[i].car_make == "BMW" || inventory[i].car_make == "Audi"){
            result.push(inventory[i]);
        }
        
    }


    if(result.length < 1){
        console.log("There are no cars from BMW or Audi in the inventory.");
        return result;
    }    
    
    console.log("These are the cars from BMW and Audi.")
    console.log(JSON.stringify(result));
    return result;

}


module.exports = getBMWAndAudi;