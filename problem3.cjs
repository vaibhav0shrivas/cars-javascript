// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.



const inventory = require('./cars.cjs');

  
  

function sortInventoryByModel(inventory){

    let Models = [];
    if(inventory.length<1){
        console.log("Car inventory is empty, no years to display.")
        return Models;
    }
    for( let i =0; i< inventory.length;i++){
        if (inventory[i].car_model in Models){
            continue;
        }
        else{
            Models.push(inventory[i].car_model)
        }
    }


    Models.sort();

    console.log("The Car Models are : ");
    console.log(Models);
    return Models;

}


module.exports = sortInventoryByModel;