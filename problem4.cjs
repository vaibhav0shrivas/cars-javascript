// ==== Problem #4 ====
// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.

const inventory = require('./cars.cjs');

  
  

function allCarYears(inventory){

    let yearsOfAllCars = [];
    if(inventory.length<1){
        console.log("Car inventory is empty, no years to display.")
        return yearsOfAllCars;
    }

    for( let i =0; i< inventory.length;i++){
        
        yearsOfAllCars.push(inventory[i].car_year);
        
    }


    console.log("The Car Years are : ");
    console.log(yearsOfAllCars);
    return yearsOfAllCars;

}


module.exports = allCarYears;